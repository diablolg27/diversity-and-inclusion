# Diversity, Inclusion and Belonging - Consulting

Use this template when you need DIB Consulting for your group.  Consutling consist of:
* Determining what types of training needed for your group
* Discussion on DIB related issues within a group
* Assistance in identifying group goals for DIB 
* Review of current processes to include DIB insights
* Discussion to determine DIB needs

## Issue creation steps

* [ ] Title the issue **Consulting Needed** this can be updated later to be more specific with the need
* [ ] Add the label DIB Consulting
* [ ] Add the date needed by in bold - please advise if there are any urgencies


## DIB Team To Do
* [ ] Assign to yourself and respond in the issue acknowledging receipt
* [ ] Set to discuss the issue 
